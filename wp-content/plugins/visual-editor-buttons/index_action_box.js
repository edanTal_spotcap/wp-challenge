(function ($) {
  $ = jQuery;

  tinymce.create("tinymce.plugins.spotcap_action_box_plugin", {

    //url argument holds the absolute url of our plugin directory
    init: function (editor, url) {
      //add new button     
      editor.addButton("action_box", {
        //image : "/wp-content/plugins/visual-editor-buttons/icons/action_button_logo.png",
        type: "menubutton",
        text: "Add action box",
        icon: false,
        menu: [
          {
            text: "add new action button",
            onclick: function () {
              // set up basic iframe html demo
              var domain = window.location.origin;

              //check if iframe has already been created
              if ($('#actionBoxForm')) {
                var iframe = "<div id='actionBoxOverlay' style='position: fixed; top: 0; left: 0;z-index:999998;width:100vw;height:100vh;background-color:rgba(0,0,0,.7)'><a id='closeActionFormBox' style='position: absolute; top: 10px; right: 10px; padding: 10px;color: #fff; cursor: pointer; font-size: 16px; text-transform: uppercase'>close box</a></div><iframe id='actionBoxForm' src='/wp-content/plugins/visual-editor-buttons/blogActionButton.php' style='position: fixed;top: 50%;left: 50%;z-index: 999999;background-color: #fff;transform: translate(-50%,-50%);min-width: 600px;min-height: 550px;'></iframe>";
                $('body').append(iframe);
                $('body').on('click', '#closeActionFormBox', function () {
                  $('#actionBoxForm, #actionBoxOverlay').css('display', 'none')
                })
              } else {
                $('#actionBoxForm, #actionBoxOverlay').css('display', 'block')
              }
              // let iframe load and then send test request
              setTimeout(function () {
                var target = document.getElementById('actionBoxForm').contentWindow;
                target.postMessage('hello', domain)
              }, 5000)

              window.addEventListener('message', function (event) {
                var actionBoxSettings = event.data;

                if (actionBoxSettings.title != undefined) {
                  editor.insertContent('[action-box title="' + actionBoxSettings.title + '" subtitle="' + actionBoxSettings.subtitle + '" url="' + actionBoxSettings.linkUrl + '" linktext="' + actionBoxSettings.linkText + '" size="' + actionBoxSettings.size + '" openLinkInNewTab="' + actionBoxSettings.linkDirection + '"]')

                  $('#actionBoxForm, #actionBoxOverlay').css('display', 'none')
                }

              }, false)
            }
          }
        ]
      });

      //button functionality.
      editor.addCommand("show_action_box", function () {
        //editor.execCommand("mceInsertContent", 0, shortcode);
        var selected_text = editor.selection.getContent();
      });

    },

    createControl: function (n, cm) {
      return null;
    },

    getInfo: function () {
      return {
        longname: "Custom shortcodes",
        author: "Bert Coppens",
        version: "1"
      };
    }
  });

  tinymce.PluginManager.add("spotcap_action_box_plugin", tinymce.plugins.spotcap_action_box_plugin);
})(jQuery);