(function() {
    tinymce.create("tinymce.plugins.spotcap_tweet_box_plugin", {

        //url argument holds the absolute url of our plugin directory
        init : function(editor, url) {
            //add new button     
            editor.addButton("tweet_box", {
                title : "Add a tweet box",
                cmd : "insert_tweet_box",
                image : "/wp-content/plugins/visual-editor-buttons/icons/Twitter_Logo_Blue.png",
            });

            //button functionality.
            editor.addCommand("insert_tweet_box", function() {
                var selected_text = editor.selection.getContent();
                var shortcode = "[tweet-box text='"+ selected_text +"']";
                editor.execCommand("mceInsertContent", 0, shortcode);
            });

        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : "Custom shortcodes",
                author : "Bert Coppens",
                version : "1"
            };
        }
    });

    tinymce.PluginManager.add("spotcap_tweet_box_plugin", tinymce.plugins.spotcap_tweet_box_plugin);
})();