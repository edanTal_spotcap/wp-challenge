<?php

/*
Plugin Name: Spotcap custom editor buttons
Plugin URI: https://www.spotcap.com 
Description: Adds button to the visual editor for our custom tweet box shortcode
Author: Bert Coppens
*/

function enqueue_plugin_scripts($plugin_array) {
    //enqueue TinyMCE plugin script with its ID.
    $plugin_array["spotcap_tweet_box_plugin"] =  plugin_dir_url(__FILE__) . "index_tweet_box.js";
    $plugin_array["spotcap_action_box_plugin"] =  plugin_dir_url(__FILE__) . "index_action_box.js";
    
    return $plugin_array;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts");

function register_tweet_box_editor($buttons)
{
    //register buttons with their id.
    array_push($buttons, "tweet_box");
    array_push($buttons, "action_box");
    return $buttons;
}

add_filter("mce_buttons", "register_tweet_box_editor");