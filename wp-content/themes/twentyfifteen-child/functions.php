<?php
require('sendgrid-php/sendgrid-php.php');

function my_theme_enqueue_styles() {

    $parent_style = 'twentyfifteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

add_shortcode('multiple_referrer', 'multiple_referrer_Cb' );

function multiple_referrer_Cb ($atts) {

    // extract options from widget
    extract(shortcode_atts( array(
        'header_text'   => '',
        'intro_text'    => '',
        'error_text'    => ''
    ), $atts ));

    //enqueue required scripts (if any)

    // Complete Frontend HTML code
    $html = '';
    ob_start();
    ?>

    <h2><?php echo $header_text; ?></h2>

    <?php
    $o = ob_get_contents(); $html .= $o; ob_get_clean();

    return $html;
}

if( shortcode_exists('multiple_referrer')) {

    vc_map( array(
        "name" 				=> "Multiple referrer",
        "base" 				=> "multiple_referrer",
        "category" 			=> "Custom Component",
        "params" 			=> array(
            array(
                'type'          => 'textfield',
                'heading'       => 'Header',
                'description'	=> 'Header displayed above the element',
                'param_name'    => 'header_text',
                'value'         => 'Recommend us!',
            ),
            array(
                'type'          => 'textfield',
                'heading'       => 'Description',
                'description'	=> 'Text displayed below the header',
                'param_name'    => 'intro_text',
                'value'         => 'Spread (y)our love!',
            ),
            array(
                'type'          => 'textfield',
                'heading'       => 'General error message',
                'description'	=> 'Text when something wrong happened',
                'param_name'    => 'error_text',
                'value'         => 'Ups! Something wrong happened. Try again later.',
            ),
        ),
    ) );

}

add_action( 'wp_ajax_nopriv_send_email', 'send_email' );

function send_email() {

    $receiver = $_POST['email'];

    $email = new \SendGrid\Mail\Mail();

    $email->setFrom($receiver);
    $email->setSubject("FE-Challenge success!");
    $email->addTo("test@fe-challenge.com", "FE Challenge");
    $email->setTemplateId('d-2e811539928740d9bce96dc5763c6b38');

    $sendgrid = new \SendGrid('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');
    try {
        $response = $sendgrid->send($email);
        echo json_encode(array(('status_code') => $response->statusCode(), 'error' => false));
        //print $response->statusCode() . "\n";
        //print_r($response->headers());
        //print $response->body() . "\n";
    } catch (Exception $e) {
        echo json_encode(array(('status_code') => '', 'error' => $e->getMessage()));
    }

    wp_die();
}


?>